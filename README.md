**How to run this app**

after cloning this repo place on folder project open a terminal and do:

cd .\functions\

npm i -> to install dependencies

on this same route:

- place your credential file provided by google firebase
rename the file name must be: permitions-firebase.json

- then you need to create a ".env" with a property:
DATEBASE_URL_FIREBASE= here goes your sdk firebase admin database URL

finally for run this app type: firebase serve

Thanks.