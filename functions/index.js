const functions = require("firebase-functions");
const express = require('express');


const app = express();

app.use(require('./routes/pacientes-routes'))

exports.app = functions.https.onRequest(app);


