const { Router } = require('express');
const { app } = require('firebase-admin');
const admin = require('firebase-admin');
const logger = require('../logger');

const router = Router()

var serviceAccount = require('../permitions-firebase.json');


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.DATEBASE_URL_FIREBASE
});

const db = admin.firestore();

router.get('/pacientes', async (req, res) => {
    logger.info(`${req.url} - Method: GET - Service: GET ALL PACIENTES`);

    try {

        const query = db.collection('pacientes');
        const querySnapshot = await query.get();
        const docs = querySnapshot.docs;

        const response = docs.map(doc => ({

            id: doc.id,
            nombre: doc.data().nombre,
            apellido_paterno: doc.data().apellido_paterno,
            apellido_materno: doc.data().apellido_materno,
            numero_de_seguridad_social: doc.data().numero_de_seguridad_social,
            accesible: doc.data().accesible

        }));

        return res.status(200).json(response);

    } catch (error) {

        return res.status(500).json(error);
    }

})

router.get('/pacientes/:id_paciente', async (req, res) => {
    logger.info(`${req.url} - Method: GET - Service: GET PACIENTE BY ID`);

    try {
        const doc = db.collection('pacientes').doc(req.params.id_paciente)
        const item = await doc.get()
        const response = item.data()
        if (response.accesible === false) {
            return res.status(403).json({ message: 'can not retrieve paciente because accesible value is false' })
        }
        return res.status(200).json(response)
    } catch (error) {
        return res.status(500).send(error)
    }
});

router.post('/pacientes', async (req, res) => {
    logger.info(`${req.url} - Method: POST - Service: CREATE PACIENTE`);

    try {
        await db.collection('pacientes')
            .doc()
            .create({
                nombre: req.body.nombre,
                apellido_paterno: req.body.apellido_paterno,
                apellido_materno: req.body.apellido_materno,
                numero_de_seguridad_social: req.body.numero_de_seguridad_social,
                accesible: req.body.accesible
            });
        return res.status(201).json();
    } catch (error) {
        return res.status(500).json(error);
    }

})
module.exports = router